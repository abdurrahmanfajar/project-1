@extends('read.main')

@section('title')
DATA KELAS
@endsection

@push('script')
<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.js"></script>
@endpush

@section('content')
<table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>No</th>
            <th>Nama Sekolah</th>
            <th>NPSN</th>
            <th>NIS</th>
            <th>NSS</th>
            <th>Alamat</th>
            <th>Kode POS</th>
            <th>Kelurahan</th>
            <th>Kecamatan</th>
            <th>Kota</th>
            <th>Provinsi</th>
            <th>Website</th>
            <th>Email</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <th>1</th>
            <th>SDIT Al Manar</th>
            <th>20109087</th>
            <th>101190</th>
            <th>640705101190</th>
            <th>Jl. Pondok Kelapa Selatan</th>
            <th>13450</th>
            <th>Pondok Kelapa</th>
            <th>Duren Sawit</th>
            <th>Jakarta Timur</th>
            <th>DKI Jakarta</th>
            <th>www.almanar.sch.id</th>
            <th>sditjkt@almanar.sch.id</th>
          </tr>
          </tbody>
        </table>
@endsection