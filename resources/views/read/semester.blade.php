@extends('read.main')

@section('title')
DATA SEMESTER
@endsection

@push('script')
<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.js"></script>
@endpush

@section('content')
<table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>No</th>
            <th>Semester</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>1</td>
            <td>Ganjil</td>
          </tr>
          </tbody>
        </table>
@endsection