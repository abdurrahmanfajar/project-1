@extends('read.main')

@section('title')
DATA MATA PELAJARAN
@endsection

@push('script')
<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.js"></script>
@endpush

@section('content')
<table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>No</th>
            <th>Tahun Ajaran</th>
            <th>Mata Pelajaran</th>
            <th>Kelas</th>
            <th>Tema/BAB</th>
            <th>No. KD-P</th>
            <th>Narasi KD-P</th>
            <th>No. KD-K</th>
            <th>Narasi KD-K</th>
            <th>Guru</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>1</td>
            <td>2021 - 2022</td>
            <td>Tahsin</td>
            <td>6</td>
            <td>Makhorijul Huruf</td>
            <td>3.1</td>
            <td>Menguasai Makhorijul Huruf dengan baik</td>
            <td>4.1</td>
            <td>Mempraktekkan Makhorijul Huruf dengan baik</td>
            <td>Fajar Abdurrahman</td>
          </tr>
          </tbody>
        </table>
@endsection