@extends('read.main')

@section('title')
DATA SISWA
@endsection

@push('script')
<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.js"></script>
@endpush

@section('content')
<table id="example1" class="table table-bordered table-striped" >
          <thead>
          <tr>
            <th>No</th>
            <th>Nama Sekolah</th>
            <th>Tahun Ajaran</th>
            <th>Nama Siswa</th>
            <th>NIS</th>
            <th>NISN</th>
            <th>TTL</th>
            <th>Jenis Kelamin</th>
            <th>Agama</th>
            <th>Pendidikan Sebelumnya</th>
            <th>Alamat</th>
            <th>Nama Ayah</th>
            <th>Nama Ibu</th>
            <th>Pekerjaan Ayah</th>
            <th>Pekerjaan Ibu</th>
            <th>Alamat Jalan</th>
            <th>Kelurahan/Desa</th>
            <th>Kecamatan</th>
            <th>Kabupaten/Kota</th>
            <th>Provinsi</th>
            <th>Nama Wali</th>
            <th>Pekerjaan Wali</th>
            <th>Alamat Wali</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>1</td>
            <td>SDIT Al Manar</td>
            <td>2021 - 2022</td>
            <td>Fajar Abdurrahman</td>
            <td>11111</td>
            <td>2222222</td>
            <td>Jakarta, 19 Mei 1990</td>
            <td>L</td>
            <td>Islam</td>
            <td>-</td>
            <td>Jl. H. Jebot Kp. Pedurenan</td>
            <td>Taufiq</td>
            <td>Siti</td>
            <td>Pensiun</td>
            <td>Ibu Rumah Tangga</td>
            <td>Jl. H. Jebot Kp. Pedurenan</td>
            <td>Jatiluhur</td>
            <td>Jatiasih</td>
            <td>Bekasi</td>
            <td>Jawa Barat</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
          </tr>
          </tbody>
        </table>
@endsection