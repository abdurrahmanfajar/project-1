<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('read.main');
});

Route::get('/login', function () {
    return view('read.login');
});

Route::get('/sekolah', function () {
    return view('read.sekolah');
});

Route::get('/tahunajaran', function () {
    return view('read.tahunajaran');
});

Route::get('/semester', function () {
    return view('read.semester');
});

Route::get('/guru', function () {
    return view('read.guru');
});

Route::get('/kelas', function () {
    return view('read.kelas');
});

Route::get('/matapelajaran', function () {
    return view('read.matapelajaran');
});

Route::get('/kdp', function () {
    return view('read.kdp');
});

Route::get('/kdk', function () {
    return view('read.kdk');
});

Route::get('/siswa', function () {
    return view('read.siswa');
});

Route::get('/latihan', function () {
    return view('read.latihan');
});

Route::get('/phki3', function () {
    return view('read.phki3');
});

Route::get('/phki4', function () {
    return view('read.phki4');
});

Route::get('/pts', function () {
    return view('read.pts');
});

Route::get('/pas', function () {
    return view('read.pas');
});